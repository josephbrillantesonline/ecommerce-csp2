// [SECTION] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");


// [SECTION] Server Setup
	
const app = express();
dotenv.config(); 
app.use(express.json());
const secret = process.env.CONNECTION_STRING
const port = process.env.PORT

// [SECTION] Application Routes

// [SECTION] Database Connect
mongoose.connect(secret)
let connectStatus = mongoose.connection;
connectStatus.on('open', () => console.log('Database Connected'));


// [SECTION] Gateway REsponse
app.get('/', (req, res) => {
	res.send('Welcome to zeke & jappy ecom site');
});
app.listen(port, () => console.log(`Server is running on port ${port}`));
