// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Please input product name']
	},
	description: {
		type: String
		default:
		
	},
	price: {
		type: Number
		default:
	
	},
	isActive {
		type: Boolean,
		default: true
	
	},
	createdOn: {		
		type: Date,
		default: new Date()
	};
});

// [SECTION] Model

const Product = mongoose.model("Product", productSchema);

module.exports = Product;