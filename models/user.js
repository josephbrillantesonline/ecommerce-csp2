// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Please input you username/email']
	},
	password: {
		type: String,
		required: [true, 'Please create a strong password']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
	clients: [
		userId: {
			type: String,
			required: [true, "User email is Required"]
		},
		pname: {
			type: String,
			required: [true, 'Please input product name']
		},
		pprice: {
			type: Number
			default:
		}

	]

	});


// [SECTION] Model

const User = mongoose.model("User", userSchema);

module.exports = User;
