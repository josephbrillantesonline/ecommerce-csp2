// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number
		default:
	},
	purchasedOn: {		
		type: Date,
		default: new Date()
	};
	});

// [SECTION] Model
const Order = mongoose.model("Order", orderSchema);

module.exports = Order;